﻿using CardDeck.Util;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace CardDeck.Cons
{
    class App
    {
        private readonly IConfiguration _config;
        private readonly DeckOfCards _deckOfCards;

        public App(IConfiguration config, DeckOfCards deckOfCards)
        {
            _config = config;
            _deckOfCards = deckOfCards;
        }

        public void Run()
        {
            const int RESHOW_INSTRUCTIONS_AFTER = 6;
            Console.WriteLine("Card Deck: press any key to create the deck");
            Console.ReadKey(true);

            _deckOfCards.CreateDeck();

            ShowInstructions();

            var commandCount = 0;

            while (true)
            {

                var userAction = Console.ReadLine().Trim().ToUpper();

                if (userAction == "Q")
                {
                    break;
                }

                switch (userAction)
                {
                    case "CD":
                        Console.WriteLine("Displaying Cards in Deck:");
                        Console.WriteLine(_deckOfCards.GetAllCardsForDisplay());
                        break;
                    case "D":
                        Console.WriteLine("Dealing Card:");
                        Console.WriteLine(_deckOfCards.DealOneCard());
                        break;
                    case "SH":
                        Console.WriteLine("Shuffeling Deck:");
                        _deckOfCards.ShuffleDeck();
                        break; 
                    case "RC":
                        Console.WriteLine("Recreating Deck:");
                        _deckOfCards.CreateDeck();
                        break;
                    case "S1":
                        Console.WriteLine("Sorting the deck by Card Suit Ascending and then Rank Ascending:");
                        _deckOfCards.SortBy();
                        break;
                    case "S2":
                        Console.WriteLine("Sorting the deck by Card Suit Descending and then Rank Ascending:");
                        _deckOfCards.SortBy(sortByCardSuitAsc: false);
                        break;
                    case "S3":
                        Console.WriteLine("Sorting the deck by Card Suit Ascending and then Rank Descending:");
                        _deckOfCards.SortBy(sortByRankAsc:false);
                        break;
                    case "S4":
                        Console.WriteLine("Sorting the deck by Card Suit Ascending and then Rank Descending:");
                        _deckOfCards.SortBy(sortByCardSuitAsc: false, sortByRankAsc: false);
                        break;

                    case "S5":
                        Console.WriteLine("Sorting the deck by Rank Ascending and then Card Suit Ascending:");
                        _deckOfCards.SortBy(sortByRankThenSuit:true);
                        break;
                    case "S6":
                        Console.WriteLine("Sorting the deck by Rank Descending and then Card Suit Ascending:");
                        _deckOfCards.SortBy(sortByRankThenSuit: true, sortByRankAsc: false);
                        break;
                    case "S7":
                        Console.WriteLine("Sorting the deck by Rank Ascending and then Card Suit Descending:");
                        _deckOfCards.SortBy(sortByRankThenSuit: true, sortByCardSuitAsc: false);
                        break;
                    case "S8":
                        Console.WriteLine("Sorting the deck by Rank Descending and then Card Suit Descending:");
                        _deckOfCards.SortBy(sortByRankThenSuit: true, sortByRankAsc: false, sortByCardSuitAsc: false);
                        break;

                    default:
                        var validCommand = false;
                        if (userAction.Length >= 2)
                        {
                            if (userAction.Substring(0,1) == "D")
                            {
                                var numberCardsToDealStr = userAction.Substring(1);
                                 
                                //public static bool TryParse(string s, out int result);
                                int numberCardsToDeal;
                                if (Int32.TryParse(numberCardsToDealStr, out numberCardsToDeal))
                                {
                                    validCommand = true;
                                    Console.WriteLine($"Dealing # {numberCardsToDealStr} cards.");
                                    Console.WriteLine(_deckOfCards.DealCards(numberCardsToDeal));
                                }

                            }
                        }

                        if (!validCommand)
                        {
                            Console.WriteLine("Invalid command or format.");
                        }
                        break;

                }

                if (commandCount == RESHOW_INSTRUCTIONS_AFTER)
                {
                    commandCount = 0;
                    ShowInstructions();
                }
                else {
                    commandCount += 1;
                    Console.WriteLine("----------------Enter Another Command------------------");
                }
            }
             
            Console.ReadLine();
            Console.ReadLine();
        }

        private void ShowInstructions ()
            {

            Console.WriteLine("\nCard Deck Commands:" +
               "\n  press D to Deal 1 card; " +
               "\n  press D followed by a number(s) to deal multiple cards (ie D3 to Deal 3 cards)" +
               "\n  press SH to shuffle the deck" +
               "\n  press S1 to sort the deck by Card Suit Ascending and then Rank Ascending " +
               "\n  press S2 to sort the deck by Card Suit Descending and then Rank Ascending " +
               "\n  press S3 to sort the deck by Card Suit Ascending and then Rank Descending " +
               "\n  press S4 to sort the deck by Card Suit Descending and then Rank Descending " +
               "\n  press S5 to sort the deck by Rank Ascending and then Card Suit Ascending " +
               "\n  press S6 to sort the deck by Rank Descending and then Card Suit Ascending " +
               "\n  press S7 to sort the deck by Rank Ascending and then Card Suit Descending " +
               "\n  press S8 to sort the deck by Rank Descending and then Card Suit Descending " +
               "\n  press CD to output the deck" +
               "\n  press RC to Recreate the deck" +
               "\n  press Q to quit" +
               "");
        }
    }
}
