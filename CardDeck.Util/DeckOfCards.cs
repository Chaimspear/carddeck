﻿using CardDeck.Util.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;


namespace CardDeck.Util
{
    public class DeckOfCards
    {


        public void CreateDeck()
        {
            _CardsInDeck = new List<Card>();
            foreach (CardSuits cardSuit in (CardSuits[])Enum.GetValues(typeof(CardSuits)))
            {
                for (int i = 1; i <= CARDS_IN_SUIT; i++)
                {
                    var rank = (i == 1 ? CARDS_IN_SUIT + 1 : i);
                    _CardsInDeck.Add( new Card(cardSuit, DetermineCardName(i), rank));
                }
            }
        }

        public void ShuffleDeck()
        {
            List<Card> shuffeledCardsInDeck = new List<Card>(); 

            for (int i = _CardsInDeck.Count; i > 0; i--)
            {
                shuffeledCardsInDeck.Add(_CardsInDeck[_randomNumberGenerator.Next(i)]); 
            }

            _CardsInDeck = shuffeledCardsInDeck;
        }


        public string GetAllCardsForDisplay(string seperator = ", " ) //stringBuilder would probably be better, but using string for simplicity
        {
            var allCardsForDisplay = string.Empty;
            _CardsInDeck.ForEach(card => allCardsForDisplay += card.CardNameWithSuit + seperator);
            return allCardsForDisplay;
        }


        public void SortBy(bool sortByRankThenSuit = false, bool sortBySuitThenRank = false, bool sortByRankAsc = true, bool sortByCardSuitAsc = true)
        { 
            if (!sortByRankThenSuit && !sortBySuitThenRank)
            {
                sortByRankThenSuit = true;
            }

            if (sortBySuitThenRank && sortByRankAsc && sortByCardSuitAsc)
            {
                _CardsInDeck = _CardsInDeck.OrderBy(c => c.CardType).ThenBy(c => c.Rank).ToList();
            }
            else if (sortBySuitThenRank && sortByRankAsc && !sortByCardSuitAsc)
            {
                _CardsInDeck = _CardsInDeck.OrderByDescending(c => c.CardType).ThenBy(c => c.Rank).ToList();
            }
            else if (sortBySuitThenRank && !sortByRankAsc && sortByCardSuitAsc)
            {
                _CardsInDeck = _CardsInDeck.OrderBy(c => c.CardType).OrderByDescending(c => c.Rank).ToList();
            }
            else if (sortBySuitThenRank && !sortByRankAsc && !sortByCardSuitAsc)
            {
                _CardsInDeck = _CardsInDeck.OrderByDescending(c => c.CardType).ThenByDescending(c => c.Rank).ToList();
            } 
            else if (sortByRankThenSuit && sortByRankAsc && sortByCardSuitAsc)
            {
                _CardsInDeck = _CardsInDeck.OrderBy(c => c.Rank).ThenBy(c => c.CardType).ToList();
            }
            else if (sortByRankThenSuit && !sortByRankAsc && sortByCardSuitAsc)
            {
                _CardsInDeck = _CardsInDeck.OrderByDescending(c => c.Rank).ThenBy(c => c.CardType).ToList();
            }
            else if (sortByRankThenSuit && sortByRankAsc && !sortByCardSuitAsc)
            {
                _CardsInDeck = _CardsInDeck.OrderBy(c => c.Rank).OrderByDescending(c => c.CardType).ToList();
            }
            else if (sortByRankThenSuit && !sortByRankAsc && !sortByCardSuitAsc)
            {
                _CardsInDeck = _CardsInDeck.OrderByDescending(c => c.Rank).ThenByDescending(c => c.CardType).ToList();
            }
 
        } 
        

        public string DealOneCard()
        {
            var cardName = string.Empty;

            if (_CardsInDeck.Count > 0)
            { 
                var randomCardInd = _randomNumberGenerator.Next(_CardsInDeck.Count);
                cardName = _CardsInDeck[randomCardInd].CardNameWithSuit;

                _CardsInDeck.RemoveAt(randomCardInd);
            }
            return cardName;
        }

        public string DealCards(int count, string seperator = ", ")
        { 
            var selectedcardNames = string.Empty;

            var dealCardsCount = Math.Min(count, _CardsInDeck.Count);

            for (int i = dealCardsCount; i > 0; i--)
            {
                var randomCardInd = _randomNumberGenerator.Next(_CardsInDeck.Count);
                selectedcardNames += _CardsInDeck[randomCardInd].CardNameWithSuit + seperator;
                _CardsInDeck.RemoveAt(randomCardInd);
            }

            return selectedcardNames;
        }

        //public List<string> DealCards(int count)
        //{
        //    List<string> cardNameList = new List<string>();

        //    var dealCardsCount = Math.Min(count, _CardsInDeck.Count);

        //    for (int i = dealCardsCount; i > 0; i--)
        //    {
        //        var randomCardInd = _randomNumberGenerator.Next(_CardsInDeck.Count);
        //        cardNameList.Add( _CardsInDeck[randomCardInd].CardNameWithSuit);

        //        _CardsInDeck.RemoveAt(randomCardInd); 
        //    }

        //    return cardNameList;
        //}


        /// <summary>
        /// ///////////////////PRIVATE DECLA
        /// </summary>

        private List<Card> _CardsInDeck;

        public static int CARDS_IN_SUIT = 13;

        private static Random _randomNumberGenerator = new Random();


        //private void SortByCardSuit(bool sortByCardSuitAsc)
        //{
        //    if (sortByCardSuitAsc)
        //    {
        //        _CardsInDeck = _CardsInDeck.OrderBy(c => c.CardType).ToList();
        //    }
        //    else
        //    {
        //        _CardsInDeck = _CardsInDeck.OrderByDescending(c => c.CardType).ToList();
        //    }
        //}

        //private void SortByRank(bool sortByRankAsc)
        //{
        //    if (sortByRankAsc)
        //    {
        //        _CardsInDeck = _CardsInDeck.OrderBy(c => c.Rank).ToList();
        //    }
        //    else
        //    {
        //        _CardsInDeck = _CardsInDeck.OrderByDescending(c => c.Rank).ToList();
        //    }
        //}

        private string DetermineCardName(int cardNo)
        {
            var cardName = string.Empty;

            switch (cardNo)
            {
                case 1:
                    {
                        cardName = "A";
                        break;
                    }
                case int n when (n >= 2 && n <= 10): 
                    {
                        cardName = cardNo.ToString();
                        break;
                    }
                case 11:
                    {
                        cardName = "Jack";
                        break;
                    }
                case 12:
                    {
                        cardName = "Queen";
                        break;
                    }
                case 13:
                    {
                        cardName = "King";

                        break;
                    } 
            }

            return cardName;
        }
    }
}
