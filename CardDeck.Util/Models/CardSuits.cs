﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CardDeck.Util.Models
{
    public enum CardSuits  
    {
        Clubs, Diamonds, Hearts, Spades
    };
}
