﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CardDeck.Util.Models
{
    class Card
    {
        public Card(CardSuits cardType, string cardName, int rank)
        {
            CardType = cardType;
            CardName = cardName;
            Rank = rank;
        }

        public CardSuits CardType { get; private set; }

        public string CardName { get; private set; } 

        public int Rank { get; private set; }

        public string CardNameWithSuit{ get {
                return CardName + " " + CardType.ToString();
} }
    }
}
